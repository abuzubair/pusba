<?php

function setActive($path)
{
    return Request::is($path) ? 'm-menu__item--active' : '';
}

function displayNameId($id)
{
    return displayName(\App\User::find($id));
}

function displayName($user)
{
    if(!$user){
        return;
    }
    return $user->nrp . ' - ' . $user->site()->first()->name . ' - ' . $user->name;
}

function sbStatus($status)
{
    if ($status == 0) {
        return 'Draft';
    }

    if ($status == 1 || $status == -1) {
        return 'Under Review';
    }

    if ($status == 2) {
        return 'Accepted';
    }

    if ($status == 3) {
        return 'Rejected';
    }
}

function sbProgress($progress, $status)
{
    if ($progress > 5) {
        return 'Finished';
    }
    
    if ($status == 1 || $status == -1) {
        return 'View';
    }


    if ($progress == 0) {
        return 'Edit';
    }

    if ($progress == 1) {
        return 'Registrasi';
    }

    if ($progress == 2) {
        return 'Analisa';
    }

    if ($progress == 3) {
        return 'Perencanaan';
    }

    if ($progress == 4) {
        return 'Pelaksanaan';
    }

    if ($progress == 5) {
        return 'Evaluasi';
    }
}

function sbLinkProgress($progress)
{
    if ($progress == 0 || $progress == 1) {
        return 'registrasi';
    }

    if ($progress == 2) {
        return 'analisa';
    }

    if ($progress == 3) {
        return 'perencanaan';
    }

    if ($progress == 4) {
        return 'pelaksanaan';
    }

    if ($progress == 5) {
        return 'evaluasi';
    }
}

function checkOption($originDatas, $originKey, $localValue)
{
    foreach ($originDatas as $originData) {
        if ($originData->$originKey == $localValue){
            return 'checked';
        }
    }
}

function roleName($role)
{
    if ($role == 0) {
        return 'Admin';
    } elseif ($role == 1) {
        return 'Editor';
    } else {
        return 'User';
    }
}

function eyeSlash($status)
{
    if (auth()->user()->role == 0) {
        if ($status != -1) {
            return true;
        }
    } else {
        if ($status != 1) {
            return true;
        }
    }
    
}

function displayDate($date)
{
    if (!$date) {
        return '';
    }
    return date('d/m/Y', strtotime($date));
}