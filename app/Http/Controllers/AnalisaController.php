<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SelBudaya;
use App\BudayaKerja;
use App\Document;

class AnalisaController extends Controller
{
    function edit($id)
    {
        $budayaKerjas = BudayaKerja::all();
        $selBudaya = SelBudaya::with('analisa', 'document')->find($id);
        return view('sels.analisa', compact('selBudaya', 'budayaKerjas'));
    }

    function update(Request $request, $id)
    {
        $this->validate($request, [
            'analysis' => 'required',
            'behaviour' => 'required',
            'impact' => 'required'
        ]);
        
        $selBudaya = SelBudaya::find($id);

        $selBudaya->analisa()->first()->update($request->all());
        $selBudaya->update([
            'status' => 1
        ]);

        return redirect('/sel-budaya');
    }
}
