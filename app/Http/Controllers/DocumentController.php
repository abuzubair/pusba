<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    function store(Request $request, $id)
    {
        $clientName = strtolower(str_replace(' ', '', $request->file->getClientOriginalName()));

        $fileName = $id . '-' . str_random(5) . '-' . $clientName;
        $path = $request->file('file')->storeAs('public/documents', $fileName);
        $uri = Storage::url($path);

        $this->validate($request, [
            'deskripsi' => 'required',
        ]);

        Document::create([
            'sel_budaya_id' => $id,
            'path' => $path,
            'uri' => $uri,
            'deskripsi' => $request->deskripsi,
            'proses' => $request->proses
        ]);

        return back();
    }

    function destroy($id)
    {
        $document = Document::find($id);
        Storage::delete($document->path);
        $document->delete();

        return back();
    }
}