<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SelBudaya;
use App\BudayaKerja;

class EvaluasiController extends Controller
{
    function edit($id){

        $selBudaya = SelBudaya::with('analisa', 'perencanaan', 'pelaksanaan', 'evaluasi')->find($id);
        $budayaKerjas = BudayaKerja::all();

        return view('sels.evaluasi', compact('selBudaya', 'budayaKerjas'));
    }

    function update(Request $request, $id)
    {
        $selBudaya = SelBudaya::find($id);
        $evaluasi = $selBudaya->evaluasi;

        $this->validate($request, [
            'sebelum' => 'required',
            'sesudah' => 'required',
            'standar' => 'required'
        ]);

        $evaluasi->update([
            'sebelum' => $request['sebelum'],
            'sesudah' => $request['sesudah'],
            'standar' => $request['standar']
        ]);

        $selBudaya->update([
            'status' => 1
        ]);
        
        return redirect('/sel-budaya');
    }
}
