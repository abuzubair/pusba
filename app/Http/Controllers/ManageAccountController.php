<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\User;

class ManageAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $userId = Auth::id();
        $users = User::where('id', '!=', $userId)->get();

        return view('accounts.manage', compact('users'));
    }

    public function store(Request $request)
    {
        
        $input = $this->validate($request,[
            'nrp' => 'required|unique:users,nrp',
            'name' => 'required'
        ]);

        $user = User::create([
            'name' => $input['name'],
            'nrp' => $input['nrp'],
            'email' => 'email@example.com',
            'password' => bcrypt('1234'),
            'role' => 1,
            'site_id' => 1,
        ]);

        return redirect('/manage-accounts');
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $input = $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'role' => 'required'
        ]);

        $user->update([
            'name' => $input['name'],
            'email' => $input['email'],
            'role' => $input['role']
        ]);
        
        return redirect('/manage-accounts');
    }

    public function destroy($id)
    {
        $user = User::find($id);

        User::destroy($id);

        return redirect('/manage-accounts');
        
    }

    public function resetPassword($id)
    {
        $user = User::find($id);
        $user->update([
            'password' => bcrypt('1234')
        ]);
        
        return redirect('/manage-accounts');
        
    }
}
