<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SelBudaya;
use App\BudayaKerja;

class PelaksanaanController extends Controller
{
    function edit($id){

        $selBudaya = SelBudaya::with('analisa', 'perencanaan', 'pelaksanaan')->find($id);
        $budayaKerjas = BudayaKerja::all();

        return view('sels.pelaksanaan', compact('selBudaya', 'budayaKerjas'));
    }

    function update(Request $request, $id)
    {
        $selBudaya = SelBudaya::find($id);
        $pelaksanaan = $selBudaya->pelaksanaan;

        $this->validate($request, [
            'tantangan' => 'required',
            'solusi' => 'required'
        ]);

        $pelaksanaan->update([
            'tantangan' => $request['tantangan'],
            'solusi' => $request['solusi']
        ]);

        $selBudaya->update([
            'status' => 1
        ]);
        
        return redirect('/sel-budaya');
    }
}
