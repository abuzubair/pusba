<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SelBudaya;
use App\BudayaKerja;
use App\Perencanaan;
use Carbon\Carbon;

class PerencanaanController extends Controller
{
    function edit($id)
    {
        $selBudaya = SelBudaya::with('analisa', 'perencanaan')->find($id);
        $budayaKerjas = BudayaKerja::all();

        return view('sels.perencanaan', compact('selBudaya', 'budayaKerjas'));
    }

    function update(Request $request, $id)
    {
        $selBudaya = SelBudaya::find($id);
        $perencanaan = $selBudaya->perencanaan;

        $this->validate($request, [
            'analisa' => 'required',
            'rencana' => 'required',
            'pihak-terlibat' => 'required'
        ]);

        $perencanaan->update([
            'analisa' => $request['analisa'],
            'rencana' => $request['rencana'],
            'pihak' => $request['pihak-terlibat']
        ]);

        $selBudaya->update([
            'status' => 1
        ]);
                
        return redirect('/sel-budaya');
    }

    function time(Request $request, $id){
        
        $selBudaya = SelBudaya::find($id);
        $perencanaan = $selBudaya->perencanaan;


        $this->validate($request, [
            'project-start' => 'required',
            'project-end' => 'required',
            'analisa-start' => 'required',
            'analisa-end' => 'required',
            'perencanaan-start' => 'required',
            'perencanaan-end' => 'required',
            'pelaksanaan-start' => 'required',
            'pelaksanaan-end' => 'required',
            'evaluasi-start' => 'required',
            'evaluasi-end' => 'required'
        ]);

        $perencanaan->update([
            'project_start' => Carbon::createFromFormat('d/m/Y', $request['project-start'])->toDateString(),
            'project_end' => Carbon::createFromFormat('d/m/Y', $request['project-end'])->toDateString(),
            'analysis_start' => Carbon::createFromFormat('d/m/Y', $request['analisa-start'])->toDateString(),
            'analysis_end' => Carbon::createFromFormat('d/m/Y', $request['analisa-end'])->toDateString(),
            'planning_start' => Carbon::createFromFormat('d/m/Y', $request['perencanaan-start'])->toDateString(),
            'planning_end' => Carbon::createFromFormat('d/m/Y', $request['perencanaan-end'])->toDateString(),
            'execution_start' => Carbon::createFromFormat('d/m/Y', $request['pelaksanaan-start'])->toDateString(),
            'execution_end' => Carbon::createFromFormat('d/m/Y', $request['pelaksanaan-end'])->toDateString(),
            'evaluation_start' => Carbon::createFromFormat('d/m/Y', $request['evaluasi-start'])->toDateString(),
            'evaluation_end' => Carbon::createFromFormat('d/m/Y', $request['evaluasi-end'])->toDateString()

            
        ]);       

        return redirect("/sel-budaya/perencanaan/$id#charthead");
    }

    function time2(Request $request, $id){
        
        $selBudaya = SelBudaya::find($id);
        $perencanaan = $selBudaya->perencanaan;


        $this->validate($request, [
            'project-start' => 'required',
            'project-end' => 'required',
            'analisa-start' => 'required',
            'analisa-end' => 'required',
            'perencanaan-start' => 'required',
            'perencanaan-end' => 'required',
            'pelaksanaan-start' => 'required',
            'pelaksanaan-end' => 'required',
            'evaluasi-start' => 'required',
            'evaluasi-end' => 'required'
        ]);

        $perencanaan->update([
            'a_project_start' => Carbon::createFromFormat('d/m/Y', $request['project-start'])->toDateString(),
            'a_project_end' => Carbon::createFromFormat('d/m/Y', $request['project-end'])->toDateString(),
            'a_analysis_start' => Carbon::createFromFormat('d/m/Y', $request['analisa-start'])->toDateString(),
            'a_analysis_end' => Carbon::createFromFormat('d/m/Y', $request['analisa-end'])->toDateString(),
            'a_planning_start' => Carbon::createFromFormat('d/m/Y', $request['perencanaan-start'])->toDateString(),
            'a_planning_end' => Carbon::createFromFormat('d/m/Y', $request['perencanaan-end'])->toDateString(),
            'a_execution_start' => Carbon::createFromFormat('d/m/Y', $request['pelaksanaan-start'])->toDateString(),
            'a_execution_end' => Carbon::createFromFormat('d/m/Y', $request['pelaksanaan-end'])->toDateString(),
            'a_evaluation_start' => Carbon::createFromFormat('d/m/Y', $request['evaluasi-start'])->toDateString(),
            'a_evaluation_end' => Carbon::createFromFormat('d/m/Y', $request['evaluasi-end'])->toDateString()
        ]);       

        return redirect("/sel-budaya/evaluasi/$id#charthead");
    }

    function getTime($id){

        $perencanaan = Perencanaan::where('sel_budaya_id', '=', $id)->first();
        $chartData = [
            [
                "category" => "Menentukan Project",
                "segments" => [[
                    "start" => $perencanaan->project_start,
                    "end" => $perencanaan->project_end,
                    "color" => "#2f4074",
                    "task" => "Menentukan Project"
                ]]
            ],
            [
                "category" => "Aktual",
                "segments" => [[
                    "start" => $perencanaan->a_project_start ? $perencanaan->a_project_start : $perencanaan->project_start,
                    "end" => $perencanaan->a_project_end ? $perencanaan->a_project_end : $perencanaan->project_start,
                    "color" => "#f4ee42",
                    "task" => "Aktual"
                ]]
            ],
            [
                "category" => "Analisa Masalah",
                "segments" => [[
                    "start" => $perencanaan->analysis_start,
                    "end" => $perencanaan->analysis_end,
                    "color" => "#2f4074",
                    "task" => "Analisa Masalah"
                ]]
            ],
            [
                "category" => "Aktual",
                "segments" => [[
                    "start" => $perencanaan->a_analysis_start ? $perencanaan->a_analysis_start : $perencanaan->project_start,
                    "end" => $perencanaan->a_analysis_end ? $perencanaan->a_analysis_end : $perencanaan->project_start,
                    "color" => "#f4ee42",
                    "task" => "Aktual"
                ]]
            ],
            [
                "category" => "Menentukan Ide",
                "segments" => [[
                    "start" => $perencanaan->planning_start,
                    "end" => $perencanaan->planning_end,
                    "color" => "#2f4074",
                    "task" => "Menentukan Ide"
                ]]
            ],
            [
                "category" => "Aktual",
                "segments" => [[
                    "start" => $perencanaan->a_planning_start ? $perencanaan->a_planning_start : $perencanaan->project_start,
                    "end" => $perencanaan->a_planning_end ? $perencanaan->a_planning_end : $perencanaan->project_start,
                    "color" => "#f4ee42",
                    "task" => "Aktual"
                ]]
            ],
            [
                "category" => "Melaksanakan Ide",
                "segments" => [[
                    "start" => $perencanaan->execution_start,
                    "end" => $perencanaan->execution_end,
                    "color" => "#2f4074",
                    "task" => "Melaksanakan Ide"
                ]]
            ],
            [
                "category" => "Aktual",
                "segments" => [[
                    "start" => $perencanaan->a_execution_start ? $perencanaan->a_execution_start : $perencanaan->project_start,
                    "end" => $perencanaan->a_execution_end ? $perencanaan->a_execution_end : $perencanaan->project_start,
                    "color" => "#f4ee42",
                    "task" => "Aktual"
                ]]
            ],
            [
                "category" => "Evaluasi",
                "segments" => [[
                    "start" => $perencanaan->evaluation_start,
                    "end" => $perencanaan->evaluation_end,
                    "color" => "#2f4074",
                    "task" => "Evaluasi"
                ]]
            ],
            [
                "category" => "Aktual",
                "segments" => [[
                    "start" => $perencanaan->a_evaluation_start ? $perencanaan->a_evaluation_start : $perencanaan->project_start,
                    "end" => $perencanaan->a_evaluation_end ? $perencanaan->a_evaluation_end : $perencanaan->project_start,
                    "color" => "#f4ee42",
                    "task" => "Aktual"
                ]]
            ],
        ];
        
        return response()->json($chartData);

    }
}
