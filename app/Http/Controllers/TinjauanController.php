<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\SelBudaya;
use App\BudayaKerja;

class TinjauanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $user = Auth::user();

        if($user->role == 0) {
            $selBudayas = SelBudaya::all();
        } else {
            $selBudayas = SelBudaya::where([
                ['coach', '=', $user->id]
            ])->get();
        }

        return view('tinjauans.index', compact('selBudayas'));
    }

    public function tinjau($id)
    {
        $selBudaya = SelBudaya::with('site')->find($id);
        $budayaKerjas = BudayaKerja::all();

        return view('tinjauans.tinjau', compact('selBudaya', 'budayaKerjas'));
    }

    public function pdf($id)
    {
        $selBudaya = SelBudaya::with('site')->find($id);
        $budayaKerjas = BudayaKerja::all();

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('tinjauans.pdf', array('selBudaya' => $selBudaya, 'budayaKerjas' => $budayaKerjas));

        return $pdf->stream();
        return $pdf->download('sel-budaya.pdf');

        return view('tinjauans.tinjau', compact('selBudaya', 'budayaKerjas'));
    }

    public function pdf2($id)
    {
        $selBudaya = SelBudaya::with('site')->find($id);
        $budayaKerjas = BudayaKerja::all();

        return view('tinjauans.pdf', compact('selBudaya', 'budayaKerjas'));
    }

    public function edit(Request $request, $id)
    {
        $selBudaya = SelBudaya::find($id);

        if($request->status){
            $selBudaya->update([
                'progress' => ($selBudaya->progress == 1 || $selBudaya->progress == 5) && $selBudaya->status == 1 ? $selBudaya->progress : $selBudaya->progress + 1,
                'status' => ($selBudaya->progress == 1 || $selBudaya->progress == 5) && $selBudaya->status != -1 ? -1 : 2
            ]);

        } else {
            $selBudaya->update(['status' => 3]);
        }
        return redirect('/tinjauan');
    }
}
