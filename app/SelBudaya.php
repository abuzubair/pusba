<?php

namespace App;

class SelBudaya extends Model
{
    public function site()
    {
        return $this->belongsTo('App\Site');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator');
    }

    public function chief()
    {
        return $this->belongsTo('App\User', 'chief');
    }
    
    public function coach()
    {
        return $this->belongsTo('App\User', 'coach');
    }

    public function cpsd()
    {
        return $this->belongsTo('App\User', 'cpsd');
    }

    public function members(){
        return $this->belongsToMany('App\User');
    }

    public function budayaKerjas(){
        return $this->belongsToMany('App\BudayaKerja');
    }

    public function analisa(){
        return $this->hasOne('App\Analisa');
    }

    public function perencanaan(){
        return $this->hasOne('App\Perencanaan');
    }

    public function pelaksanaan(){
        return $this->hasOne('App\Pelaksanaan');
    }

    public function evaluasi(){
        return $this->hasOne('App\Evaluasi');
    }

    public function document(){
        return $this->hasMany('App\Document');
    }

}
