<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'role', 'password', 'nrp', 'site_id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function site(){
        return $this->belongsTo('App\Site');
    }

    public function selBudayas(){
        return $this->belongsToMany('App\SelBudaya');
    }
}
