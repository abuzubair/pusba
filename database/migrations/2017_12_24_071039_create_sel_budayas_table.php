<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSelBudayasTable extends Migration
{

    public function up()
    {
        Schema::create('sel_budayas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_number')->nullable();
            $table->integer('creator')->nullable();
            $table->string('team')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('chief')->nullable();
            $table->integer('coach')->nullable();
            $table->integer('cpsd')->nullable();
            $table->string('topic')->nullable();
            $table->string('title')->nullable();
            $table->integer('progress')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });

        Schema::create('sel_budaya_user', function (Blueprint $table) {
            $table->integer('sel_budaya_id');
            $table->integer('user_id');
            $table->primary(['sel_budaya_id', 'user_id']);
        });

        Schema::create('budaya_kerja_sel_budaya', function (Blueprint $table) {
            $table->integer('budaya_kerja_id');
            $table->integer('sel_budaya_id');
            $table->primary(['budaya_kerja_id', 'sel_budaya_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('sel_budayas');
        Schema::dropIfExists('sel_budaya_user');
        Schema::dropIfExists('budaya_kerja_sel_budaya');
    }
}
