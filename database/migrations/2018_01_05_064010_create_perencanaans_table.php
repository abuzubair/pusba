<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerencanaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perencanaans', function (Blueprint $table) {
            $table->increments('id');
            $table->date('project_start')->nullable();
            $table->date('project_end')->nullable();
            $table->date('analysis_start')->nullable();
            $table->date('analysis_end')->nullable();
            $table->date('planning_start')->nullable();
            $table->date('planning_end')->nullable();
            $table->date('execution_start')->nullable();
            $table->date('execution_end')->nullable();
            $table->date('evaluation_start')->nullable();
            $table->date('evaluation_end')->nullable();
            $table->date('a_project_start')->nullable();
            $table->date('a_project_end')->nullable();
            $table->date('a_analysis_start')->nullable();
            $table->date('a_analysis_end')->nullable();
            $table->date('a_planning_start')->nullable();
            $table->date('a_planning_end')->nullable();
            $table->date('a_execution_start')->nullable();
            $table->date('a_execution_end')->nullable();
            $table->date('a_evaluation_start')->nullable();
            $table->date('a_evaluation_end')->nullable();
            $table->string('analisa')->nullable();
            $table->string('rencana')->nullable();
            $table->string('pihak')->nullable();
            $table->integer('sel_budaya_id')->unsigned();
            $table->foreign('sel_budaya_id')->references('id')->on('sel_budayas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ide_perbaikans');
    }
}
