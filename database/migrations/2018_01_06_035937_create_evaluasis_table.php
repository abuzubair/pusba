<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluasis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sebelum')->nullable();
            $table->string('sesudah')->nullable();
            $table->string('standar')->nullable();
            $table->integer('sel_budaya_id')->unsigned();
            $table->foreign('sel_budaya_id')->references('id')->on('sel_budayas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluasis');
    }
}
