<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BudayaKerjasTableSeeder::class,
            SelBudayasTableSeeder::class,
            SitesTableSeeder::class,
            UsersTableSeeder::class
        ]);
    }
}
