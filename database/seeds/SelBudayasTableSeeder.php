<?php

use Illuminate\Database\Seeder;

class SelBudayasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
   
        \App\SelBudaya::insert([
            [
                'reg_number'=>'SB/IBPL/2018/1',
                'creator'=>2,
                'team'=>'Majapahit',
                'site_id'=>3,
                'chief'=>3,
                'coach'=>4,
                'cpsd'=>5,
                'topic'=>'Suscipit cupiditate molestias reprehenderit ea necessitatibus magni dolorum.',
                'title'=>'Et blanditiis voluptates est autem animi possimus ducimus.',
                'progress'=>1,
                'status'=>1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]
        ]);

        \App\Evaluasi::insert([
            [
                'sebelum' => 'Sebelum',
                'sesudah' => 'Sesudah',
                'standar' => 'Standarisasi',
                'sel_budaya_id'=>1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]
        ]);

        \App\Pelaksanaan::insert([
            [
                'tantangan' => 'Tantangan',
                'solusi' => 'Solusi',
                'sel_budaya_id'=>1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]
        ]);

        \App\Perencanaan::insert([
            [
                'analisa' => 'Analisa',
                'rencana' => 'Rencana',
                'pihak' => 'Pihak',
                'sel_budaya_id'=>1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]
        ]);

        \App\Analisa::insert([
            [
                'analysis'=>'Voluptatem maiores tempore nulla blanditiis blanditiis.',
                'behaviour'=>'Ut laboriosam voluptates nulla quo quia aut delectus dignissimos.',
                'impact'=>'Et sed nostrum commodi in impedit libero nobis quibusdam eum.',
                'sel_budaya_id'=>1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]
        ]);

        DB::table('sel_budaya_user')->insert([
            [
                'user_id'=>2,
                'sel_budaya_id'=>1
            ],
            [
                'user_id'=>3,
                'sel_budaya_id'=>1
            ],
            [
                'user_id'=>4,
                'sel_budaya_id'=>1
            ],
            [
                'user_id'=>5,
                'sel_budaya_id'=>1
            ],
        ]);

        DB::table('budaya_kerja_sel_budaya')->insert([
            [
                'budaya_kerja_id'=>1,
                'sel_budaya_id'=>1
            ],
            [
                'budaya_kerja_id'=>2,
                'sel_budaya_id'=>1
            ],
            [
                'budaya_kerja_id'=>3,
                'sel_budaya_id'=>1
            ]
        ]);
    }
}
