<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::insert([
            [
                'name'=>'Fariz Muhamad Azhari',
                'email'=>'1@example.com',
                'nrp'=>'KB17130',
                'site_id'=>1,
                'password'=>bcrypt('1234'),
                'role'=>0,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ],
            [
                'name'=>'Luluk Muslimah',
                'email'=>'2@example.com',
                'nrp'=>'KB13075',
                'site_id'=>1,
                'password'=>bcrypt('1234'),
                'role'=>1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')               
            ],
            [
                'name'=>'Andriawan Tri Laksono',
                'email'=>'3@example.com',
                'nrp'=>'KB12163',
                'site_id'=>1,
                'password'=>bcrypt('1234'),
                'role'=>1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')               
            ],
            [
                'name'=>'Sudjianto',
                'email'=>'4@example.com',
                'nrp'=>'1C95005',
                'site_id'=>1,
                'password'=>bcrypt('1234'),
                'role'=>1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')               
            ],
            [
                'name'=>'Sarah Mulyani',
                'email'=>'5@example.com',
                'nrp'=>'1C95000',
                'site_id'=>1,
                'password'=>bcrypt('1234'),
                'role'=>1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')               
            ]
        ]);
    }
}
