# PUSBa

Pusat Utama Sel Budaya - KPP Mining. This project was build on top of [PHP](http://php.net/manual/en/install.php), [MySQL](https://www.mysql.com/), and [Laravel Framework](https://laravel.com/docs/5.5/installation).

## Server Requirements
- PHP >= 7.0.0
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

## Configuration

#### Public Directory
After installing Laravel, you should configure your web server's document / web root to be the  `public` directory. The `index.php` in this directory serves as the front controller for all HTTP requests entering your application.

#### Directory Permissions
After installing Laravel, you may need to configure some permissions. Directories within the  `storage` and the `bootstrap/cache` directories should be `writable` by your web server or Laravel will not run. If you are using the Homestead virtual machine, these permissions should already be set.

#### Configuration
Copy `.env.example` to `.env` and edit `.env`
```
APP_URL=http://pusba.invalid  [YOUR APP URL]

DB_CONNECTION=mysql
DB_HOST=localhost  [DATABASE HOST]
DB_PORT=3306
DB_DATABASE=pusba  [MYSQL DB NAME]
DB_USERNAME=root  [MYSQL USER]
DB_PASSWORD=root  [MYSQL PASSWORD]
```
#### Run This
```
$ php composer install
$ php composer dumpautoload -o
$ php artisan config:cache
$ php artisan route:cache
```
