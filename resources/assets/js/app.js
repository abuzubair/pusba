import 'amcharts3'
import 'amcharts3/amcharts/serial'
import 'amcharts3/amcharts/themes/light'
import 'amcharts3/amcharts/gantt'
import 'amcharts3/amcharts/plugins/dataloader/'

var BootstrapDatepicker = function () {
    var t = function () {

        $("#datepicker_1").datepicker({
            format: 'dd/mm/yyyy',
            // startDate: '-3d',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }),


            $("#datepicker_2").datepicker({
                format: 'dd/mm/yyyy',
                // startDate: '-3d',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }),


            $("#datepicker_3").datepicker({
                format: 'dd/mm/yyyy',
                // startDate: '-3d',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }),


            $("#datepicker_4").datepicker({
                format: 'dd/mm/yyyy',
                // startDate: '-3d',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }),


            $("#datepicker_5").datepicker({
                format: 'dd/mm/yyyy',
                // startDate: '-3d',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
    };

    return {
        init: function () {
            t()
        }
    }
}();

jQuery(document).ready(function () {
    BootstrapDatepicker.init()

    var uri = $("#time").attr("action");

    var chart = AmCharts.makeChart("chartdiv", {
        "path": "/amcharts/",
        "type": "gantt",
        "theme": "light",
        "marginRight": 70,
        "period": "DD",
        "dataDateFormat": "YYYY-MM-DD",
        "columnWidth": 0.5,
        "valueAxis": {
            "type": "date"
        },
        "brightnessStep": 7,
        "graph": {
            "fillAlphas": 1,
            "lineAlpha": 1,
            "lineColor": "#fff",
            "fillAlphas": 0.85,
            "balloonText": "<b>[[task]]</b>:<br />[[open]] -- [[value]]"
        },
        "rotate": true,
        "categoryField": "category",
        "segmentsField": "segments",
        "colorField": "color",
        "startDateField": "start",
        "endDateField": "end",
        "dataLoader": {
            "url": uri,
            "format": "json"
        },
        "valueScrollbar": {
            "autoGridCount": true
        },
        "chartCursor": {
            "cursorColor": "#55bb76",
            "valueBalloonsEnabled": false,
            "cursorAlpha": 0,
            "valueLineAlpha": 0.5,
            "valueLineBalloonEnabled": true,
            "valueLineEnabled": true,
            "zoomable": false,
            "valueZoomable": true
        },

        "export": {
            "enabled": true
        }
    });

});