@extends('layouts.body')

@section('content')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-subheader ">
          <div class="d-flex align-items-center">
              <div class="mr-auto">
                  <h3 class="m-subheader__title ">
                      Edit Account
                  </h3>
              </div>
          </div>
      </div>
      <!-- END: Subheader -->
      <div class="m-content">
        <div class="row">
                   
          <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--tab">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                      <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                      Edit Password
                    </h3>
                  </div>
                </div>
              </div>
              <!--begin::Form-->
              <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/edit-account/password" id="edit-password">
                {{ csrf_field() }}
                <div class="m-portlet__body">

                  <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">
                      Old password
                    </label>
                    <input type="password" class="form-control m-input" id="oldPassword" placeholder="Password" name="oldPassword">
                    @include('layouts.errors-form', ['field' => 'oldPassword'])                  
                  </div>
                  
                  <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">
                      New password
                    </label>
                    <input type="password" class="form-control m-input" id="password_confirmation" placeholder="Password" name="password_confirmation">
                    @include('layouts.errors-form', ['field' => 'password_confirmation'])                                    
                  </div>

                  <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">
                      Password confirmation
                    </label>
                    <input type="password" class="form-control m-input" id="password" placeholder="Password" name="password">
                    @include('layouts.errors-form', ['field' => 'password'])                                    
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="edit-password">
                      Save
                    </button>
                  </div>
                </div>
              </form>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>

        </div>
      </div>

  </div>
@endsection

@section('contentmodal')

@endsection

@section('contentscript')

    @include ('footer')

@endsection