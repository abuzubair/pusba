@extends('layouts.body')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">
                        Manage Accounts
                    </h3>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Manage Accounts
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">

                    @include('layouts.errors-alert')

                    <!--begin: Search Form -->
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span>
                                                    <i class="la la-search"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                <a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" data-toggle="modal" data-target="#newaccount">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            New Account
                                        </span>
                                    </span>
                                </a>
                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                            </div>
                        </div>
                    </div>
                    <!--end: Search Form -->
                    <!--begin: Datatable -->
                    <table class="m-datatable" id="html_table" width="100%">
                        <thead>
                            <tr>
                                <th data-field="NRP"></th>
                                <th data-field="Name"></th>
                                <th data-field="Actions"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)  
                                <tr>
                                    <td>
                                        {{ $user->nrp }}
                                    </td>
                                    <td>
                                        {{ $user->name }}
                                    </td>
                                    <td>
                                        <span>
                                            <a href="/manage-accounts/reset/{{$user->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick="return confirm('Are you sure?')" title="Reset Password">
                                                <i class="la la-key"></i>
                                            </a>
                                            <a href="/manage-accounts/delete/{{$user->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="return confirm('Are you sure?')" title="Delete">
                                                <i class="la la-trash"></i>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
            
        </div>
    </div>
@endsection

@section('contentmodal')

    <div class="modal fade" id="newaccount" tabindex="-1" role="dialog" aria-labelledby="editmodal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editmodal">
                        New Account
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/manage-accounts" id="addCourse">
                        {{ csrf_field() }}
                        <div class="m-portlet__body">
                        
                            <div class="form-group m-form__group">
                                <label for="courseCode">
                                    NRP
                                </label>
                                <input type="text" class="form-control m-input" id="courseCode" placeholder="Enter NRP"  name="nrp">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="courseName">
                                    Name
                                </label>
                                <input type="text" class="form-control m-input" id="courseName" placeholder="Enter name" name="name">
                            </div>

                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <div class="modal-footer m-form__actions">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary" form="addCourse">
                        Create
                    </button>
                </div>
            </div>
        </div>
    </div>
    

@endsection

@section('contentscript')

    <script>
   
   
        var DatatableHtmlTableDemo = function() {
        //== Private functions

            // demo initializer
            var demo = function() {

                var datatable = $('.m-datatable').mDatatable({
                data: {
                    saveState: {cookie: false},
                },
                search: {
                    input: $('#generalSearch'),
                },
                columns: [
                    {
                        field: 'Email',
                        width: 400
                        
                    },
                    {
                        field: 'Name',
                        width: 500                      
                    },
                    {
                        field: 'Role',
                        width: 200                      
                    },
                    {
                        field: "Actions",
                        sortable: false,
                        overflow: "visible",
                        width: 150,
                        textAlign: 'center' 
                    },
                ],
                });
            };

            return {
                //== Public functions
                init: function() {
                // init dmeo
                demo();
                },
            };
        }();

        jQuery(document).ready(function() {
            DatatableHtmlTableDemo.init();

            $('.m-datatable').on('click', '.course-editor' ,function () {
                $('#editAccount').attr('action', '/manage-accounts/edit/' + $(this).data('id'));
                $('#editAccount #courseCode').attr('value', $(this).data('email'));
                $('#editAccount #courseName').attr('value', $(this).data('name'));

            });

        });



        
    </script>

    @include ('footer')

@endsection