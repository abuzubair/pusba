<!--begin::Portlet-->
<div class="m-portlet m-portlet--tab">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide">
          <i class="la la-gear"></i>
        </span>
        <h3 class="m-portlet__head-text">
          Analisa
        </h3>
      </div>
    </div>
  </div>
  <!--begin::Form-->
  <form class="m-form m-form--fit m-form--label-align-right">

    <div class="m-portlet__body">

      <div class="form-group m-form__group">
        <label>
          Analisa Kondisi
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->analisa->analysis}}</textarea>
      </div>

      <div class="form-group m-form__group">
        <label>
          Perilaku Tidak Mendukung
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->analisa->behaviour}}</textarea>
      </div>

      <div class="form-group m-form__group">
        <label>
          Dampak Dari Perilaku Saat Ini (Eksternal & Internal)
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->analisa->impact}}</textarea>
      </div>

      @component('displays.document', ['document' => $selBudaya->document, 'proses' => 2, 'delete' => false])
      @endcomponent

    </div>

  </form>

  <!--end::Form-->
</div>
<!--end::Portlet-->