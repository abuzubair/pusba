<div class="form-group m-form__group">
  <label for="impact">
    Dokumen Pendukung
  </label>
  <div class="m-list-timeline">
    <div class="m-list-timeline__items">

      @foreach ($document as $item) 
      @if ($item->proses == $proses)
        <div class="m-list-timeline__item">
          <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
          <span class="m-list-timeline__text">
            {{ $item->deskripsi }}
          </span>
          <span class="m-list-timeline__time">
            <a href="{{$item->uri}}" class="btn m-btn m-btn--hover-success m-btn--icon btn-sm m-btn--icon-only m-btn--pill" title="Download">
              <i class="la la-download"></i>
            </a>
            @if ($delete)
                
              <a href="/sel-budaya/upload/delete/{{$item->id}}" class="btn m-btn m-btn--hover-danger m-btn--icon btn-sm m-btn--icon-only m-btn--pill"
                title="Delete">
                <i class="la la-trash"></i>
              </a>

            @endif
          </span>
        </div>
      @endif
      @endforeach
      
    </div>
  </div>
</div>
