<!--begin::Portlet-->
<div class="m-portlet m-portlet--tab">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide">
          <i class="la la-gear"></i>
        </span>
        <h3 class="m-portlet__head-text">
          Evaluasi
        </h3>
      </div>
    </div>
  </div>
  <!--begin::Form-->
  <form class="m-form m-form--fit m-form--label-align-right">

    <div class="m-portlet__body">

      <div class="form-group m-form__group">
        <label>
          Perilaku Sebelum
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->evaluasi->sebelum}}</textarea>
      </div>

      <div class="form-group m-form__group">
        <label>
          Perilaku Sesudah
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->evaluasi->sesudah}}</textarea>
      </div>

      <div class="form-group m-form__group">
        <label>
          Standarisasi
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->evaluasi->standar}}</textarea>
      </div>

      @component('displays.document', ['document' => $selBudaya->document, 'proses' => 5, 'delete' => false])
      @endcomponent

    </div>

  </form>

  <!--end::Form-->
</div>
<!--end::Portlet-->