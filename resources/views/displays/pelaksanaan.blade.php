<!--begin::Portlet-->
<div class="m-portlet m-portlet--tab">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide">
          <i class="la la-gear"></i>
        </span>
        <h3 class="m-portlet__head-text">
          Melaksanakan Ide Perbaikan
        </h3>
      </div>
    </div>
  </div>
  <!--begin::Form-->
  <form class="m-form m-form--fit m-form--label-align-right">

    <div class="m-portlet__body">

      <div class="form-group m-form__group">
        <label>
          Tantangan
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->pelaksanaan->tantangan}}</textarea>
      </div>

      <div class="form-group m-form__group">
        <label>
          Solusi / Strategi
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->pelaksanaan->solusi}}</textarea>
      </div>

      @component('displays.document', ['document' => $selBudaya->document, 'proses' => 4, 'delete' => false])
      @endcomponent

    </div>

  </form>

  <!--end::Form-->
</div>
<!--end::Portlet-->