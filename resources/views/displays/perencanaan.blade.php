<!--begin::Portlet-->
<div class="m-portlet m-portlet--tab">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide">
          <i class="la la-gear"></i>
        </span>
        <h3 class="m-portlet__head-text">
          Menentukan Ide Perbaikan
        </h3>
      </div>
    </div>
  </div>
  <!--begin::Form-->
  <form class="m-form m-form--fit m-form--label-align-right">

    <div class="m-portlet__body">

      <div class="form-group m-form__group">
        <label>
          Time Frame
        </label>
        <div id="data-url" data-url="/sel-budaya/time/{{$selBudaya->id}}"></div>
        @if ($selBudaya->progress<5)
            
        <div id="chartdiv2"></div>
        @endif
        <style>
            #chartdiv2 {
                width: 100%;
                height: 500px;
            }
        </style>

      </div>

      <div class="form-group m-form__group">
        <label>
          Analisa Akar Penyebab Perilaku Tidak Mendukung
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->perencanaan->analisa}}</textarea>
      </div>

      <div class="form-group m-form__group">
        <label>
          Rencana & Implementasi Solusi
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->perencanaan->rencana}}</textarea>
      </div>

      <div class="form-group m-form__group">
        <label>
          Pihak yang yang terlibat & PIC Utama Pelaksanaan Solusi
        </label>
        <textarea class="form-control m-input" rows="8" disabled>{{$selBudaya->perencanaan->pihak}}</textarea>
      </div>

    </div>

  </form>

  <!--end::Form-->
</div>
<!--end::Portlet-->