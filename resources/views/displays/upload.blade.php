<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/sel-budaya/upload/{{$selBudaya->id}}"
  id="upload" enctype="multipart/form-data">
  <div class="m-portlet__body">
    {{ csrf_field() }}

    @component('displays.document', ['document' => $selBudaya->document, 'proses' => $value, 'delete' => true])

    @endcomponent

    <input type="hidden" name="proses" value="{{ $value }}">
    <div class="form-group m-form__group">
      <label for="">
        Tambah Dokumen Pendukung
      </label>
      <div></div>
      <label class="">
        <input type="file" id="file" name="file" class="">
      </label>
    </div>

    <div class="form-group m-form__group">
      <label for="deskripsi">
        Deskripsi Dokumen
      </label>
      <textarea name="deskripsi" class="form-control m-input" id="deskripsi" rows="3" placeholder="Deskripsi dokumen"></textarea>
      @include('layouts.errors-form', ['field' => 'deskripsi'])
    </div>

    <div class="m-form__actions">
      <button type="submit" class="btn btn-success m-btn m-btn--custom float-right" form="upload">
        Upload
      </button>
    </div>
  </div>
</form>