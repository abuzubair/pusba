@if(count($errors))
    <div class="m-section__content">
    @foreach($errors->all() as $error)
        <div class="m-alert m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {{$error}}
        </div>
    @endforeach
    </div>
@endif