<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
  <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">

  <!-- BEGIN: Aside Menu -->
  <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
    
      <li class="m-menu__item  m-menu__item {{ setActive('sel-budaya*')}}" aria-haspopup="true" >
        <a  href="/sel-budaya" class="m-menu__link">
          <i class="m-menu__link-icon flaticon-line-graph"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Sel Budaya
              </span>
            </span>
          </span>
        </a>
      </li>
    
      <li class="m-menu__item  m-menu__item {{ setActive('tinjauan*')}}" aria-haspopup="true" >
        <a  href="/tinjauan" class="m-menu__link">
          <i class="m-menu__link-icon flaticon-line-graph"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Tinjauan
              </span>
            </span>
          </span>
        </a>
      </li>
          
      <li class="m-menu__section">
        <h4 class="m-menu__section-text">
          Account
        </h4>
        <i class="m-menu__section-icon flaticon-more-v3"></i>
      </li>
      @if(Auth::user()->role == 0)
          
      <li class="m-menu__item {{ setActive('manage-accounts*')}}" aria-haspopup="true">
        <a  href="/manage-accounts" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon flaticon-users"></i>
          <span class="m-menu__link-text">
            Manage Account
          </span>
        </a>
      </li>

      @endif
      <li class="m-menu__item {{ setActive('edit-account*')}}" aria-haspopup="true">
        <a  href="/edit-account" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon flaticon-users"></i>
          <span class="m-menu__link-text">
            Edit Account
          </span>
        </a>
      </li>
      <li class="m-menu__item" aria-haspopup="true">
        <a  
          href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();"
          class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon flaticon-logout"></i>
          <span class="m-menu__link-text">
            Log Out
          </span>
        </a>
        <form id="logout-form" action="/logout" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </div>
  <!-- END: Aside Menu -->
</div>