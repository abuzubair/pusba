@extends('layouts.body')

@section('content')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-subheader ">
          <div class="d-flex align-items-center">
              <div class="mr-auto">
                  <h3 class="m-subheader__title ">
                      Sel Budaya
                  </h3>
              </div>
          </div>
      </div>
      <!-- END: Subheader -->
      <div class="m-content">
        <div class="row">
          
          <div class="col-lg-6">
            @include('displays.register')

            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--tab">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                      <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                      Analisa
                    </h3>
                  </div>
                </div>
              </div>
              <!--begin::Form-->
              <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/sel-budaya/analisa/{{$selBudaya->id}}" id="analisa">
                <div class="m-portlet__body">

                {{ csrf_field() }}

                  <div class="form-group m-form__group">
                    <label for="analysis">
                      Analisa Kondisi
                    </label>
                    <textarea name="analysis" class="form-control m-input" id="analysis" rows="8">{{$selBudaya->analisa->analysis}}</textarea>
                    @include('layouts.errors-form', ['field' => 'analysis'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="behaviour">
                      Perilaku Tidak Mendukung
                    </label>
                    <textarea name="behaviour" class="form-control m-input" id="behaviour" rows="8">{{$selBudaya->analisa->behaviour}}</textarea>
                    @include('layouts.errors-form', ['field' => 'behaviour'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="impact">
                      Dampak Dari Perilaku Saat Ini (Eksternal & Internal)
                    </label>
                    <textarea name="impact" class="form-control m-input" id="impact" rows="8">{{$selBudaya->analisa->impact}}</textarea>
                    @include('layouts.errors-form', ['field' => 'impact'])
                  </div>
                </div>                  
              </form>
              
              @component('displays.upload', ['selBudaya' => $selBudaya, 'value' => 2])
              @endcomponent
                
              <div class="m-form m-form--fit m-form--label-align-right">
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="analisa">
                      Proses
                    </button>
                    <a href="/sel-budaya" class="btn btn-danger m-btn m-btn--custom float-left" role="button">Batal</a>
                  </div>
                </div>
              </div>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>

        </div>
      </div>
  </div>
@endsection

@section('contentmodal')

@endsection

@section('contentscript')

  @include ('footer')

@endsection
