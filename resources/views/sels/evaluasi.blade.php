@extends('layouts.body')

@section('content')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-subheader ">
          <div class="d-flex align-items-center">
              <div class="mr-auto">
                  <h3 class="m-subheader__title ">
                      Sel Budaya
                  </h3>
              </div>
          </div>
      </div>
      <!-- END: Subheader -->
      <div class="m-content">
        <div class="row">
          
          <div class="col-lg-6">
            @include('displays.register')
            @include('displays.analisa')
            @include('displays.perencanaan')
            @include('displays.pelaksanaan')

            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--tab">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                      <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                      Evaluasi
                    </h3>
                  </div>
                </div>
              </div>
              <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/sel-budaya/time2/{{$selBudaya->id}}" id="time">

                {{ csrf_field() }}
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-5 col-sm-12">
                            Menentukan Project Budaya KPP
                        </label>
                        <div class="col-lg-7 col-sm-12">
                            <div class="input-daterange input-group" id="datepicker_1">
                                <input type="text" class="form-control m-input" name="project-start" value="{{displayDate($selBudaya->perencanaan->a_project_start)}}"/>
                                <span class="input-group-addon">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                                <input type="text" class="form-control" name="project-end" value="{{displayDate($selBudaya->perencanaan->a_project_end)}}"/>
                            </div>
                            @include('layouts.errors-form', ['field' => 'project-start'])
                            @include('layouts.errors-form', ['field' => 'project-end'])
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-5 col-sm-12">
                            Analisa Masalah
                        </label>
                        <div class="col-lg-7 col-sm-12">
                            <div class="input-daterange input-group" id="datepicker_2">
                                <input type="text" class="form-control m-input" name="analisa-start" value="{{displayDate($selBudaya->perencanaan->a_analysis_start)}}"/>
                                <span class="input-group-addon">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                                <input type="text" class="form-control" name="analisa-end" value="{{displayDate($selBudaya->perencanaan->a_analysis_end)}}"/>
                            </div>
                            @include('layouts.errors-form', ['field' => 'analisa-start'])
                            @include('layouts.errors-form', ['field' => 'analisa-end'])
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-5 col-sm-12">
                            Menentukan Ide Perbaikan
                        </label>
                        <div class="col-lg-7 col-sm-12">
                            <div class="input-daterange input-group" id="datepicker_3">
                                <input type="text" class="form-control m-input" name="perencanaan-start" value="{{displayDate($selBudaya->perencanaan->a_planning_start)}}"/>
                                <span class="input-group-addon">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                                <input type="text" class="form-control" name="perencanaan-end" value="{{displayDate($selBudaya->perencanaan->a_planning_end)}}"/>
                            </div>
                            @include('layouts.errors-form', ['field' => 'perencanaan-start'])
                            @include('layouts.errors-form', ['field' => 'perencanaan-end'])
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-5 col-sm-12">
                            Melaksanakan Ide Perbaikan
                        </label>
                        <div class="col-lg-7 col-sm-12">
                            <div class="input-daterange input-group" id="datepicker_4">
                                <input type="text" class="form-control m-input" name="pelaksanaan-start" value="{{displayDate($selBudaya->perencanaan->a_execution_start)}}"/>
                                <span class="input-group-addon">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                                <input type="text" class="form-control" name="pelaksanaan-end" value="{{displayDate($selBudaya->perencanaan->a_execution_end)}}"/>
                            </div>
                            @include('layouts.errors-form', ['field' => 'pelaksanaan-start'])
                            @include('layouts.errors-form', ['field' => 'pelaksanaan-end'])
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-5 col-sm-12">
                            Evaluasi Hasil
                        </label>
                        <div class="col-lg-7 col-sm-12">
                            <div class="input-daterange input-group" id="datepicker_5">
                                <input type="text" class="form-control m-input" name="evaluasi-start" value="{{displayDate($selBudaya->perencanaan->a_evaluation_start)}}"/>
                                <span class="input-group-addon">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                                <input type="text" class="form-control" name="evaluasi-end" value="{{displayDate($selBudaya->perencanaan->a_evaluation_end)}}"/>
                            </div>
                            @include('layouts.errors-form', ['field' => 'evaluasi-start'])
                            @include('layouts.errors-form', ['field' => 'evaluasi-end'])
                        </div>
                    </div>

                </div>

                <div id="charthead" class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="time">
                      Update Gantt Chart
                    </button>
                  </div>
                </div>

                <div class="form-group m-form__group">

                    <div id="chartdiv"></div>
                    <style>
                        #chartdiv {
                            width: 100%;
                            height: 500px;
                        }
                    </style>

                </div>
                
              </form>
              <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/sel-budaya/evaluasi/{{$selBudaya->id}}" id="evaluasi">

                {{ csrf_field() }}
                <div class="m-portlet__body">

                  <div class="form-group m-form__group">
                    <label for="sebelum">
                       Perilaku Sebelum
                    </label>
                    <textarea name="sebelum" class="form-control m-input" id="sebelum" rows="8">{{$selBudaya->evaluasi->sebelum}}</textarea>
                    @include('layouts.errors-form', ['field' => 'sebelum'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="sesudah">
                       Perilaku Sesudah
                    </label>
                    <textarea name="sesudah" class="form-control m-input" id="sesudah" rows="8">{{$selBudaya->evaluasi->sesudah}}</textarea>
                    @include('layouts.errors-form', ['field' => 'sesudah'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="standar">
                       Standarisasi
                    </label>
                    <textarea name="standar" class="form-control m-input" id="standar" rows="8">{{$selBudaya->evaluasi->standar}}</textarea>
                    @include('layouts.errors-form', ['field' => 'standar'])
                  </div>
                </div>
              </form>
              @component('displays.upload', ['selBudaya' => $selBudaya, 'value' => 5])
              @endcomponent
              <div class="m-form m-form--fit m-form--label-align-right">
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="evaluasi">
                      Proses
                    </button>
                    <a href="/sel-budaya" class="btn btn-danger m-btn m-btn--custom float-left" role="button">Batal</a>
                  </div>
                </div>
              </div>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>

        </div>
      </div>
  </div>
@endsection

@section('contentmodal')

@endsection

@section('contentscript')

  @include ('footer')

@endsection
