@extends('layouts.body')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">
                    Dashboard
                </h3>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Sel Budaya
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                            <a href="/sel-budaya/registrasi" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>
                                        Sel Budaya
                                    </span>
                                </span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th data-field="Nomer Registrasi"></th>
                            <th data-field="Tim"></th>
                            <th data-field="Ketua"></th>
                            <th data-field="Pelatih Utama"></th>
                            <th data-field="Status"></th>
                            <th data-field="Tindak Lanjut"></th>
                            <th data-field="Hapus"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($selBudayas as $selBudaya)  
                            <tr>
                                <td>
                                    {{ $selBudaya->reg_number }}
                                </td>
                                <td>
                                    {{ $selBudaya->team }}
                                    
                                </td>
                                <td>
                                    {{ displayName($selBudaya->chief()->first()) }}
                                </td>
                                <td>
                                    {{ displayName($selBudaya->coach()->first()) }}
                                </td>
                                <td>
                                    <span>
                                        <span class="m-badge m-badge--{{$selBudaya->status == 0 || $selBudaya->status == 1 ? 'metal' : ''}}{{$selBudaya->status == 2 ? 'success' : ''}}{{$selBudaya->status == 3 ? 'danger' : ''}} m-badge--wide">{{ $selBudaya->progress > 5 ? 'Finished' : sbStatus($selBudaya->status) }}</span>
                                    </span>
                                </td>
                                <td>
                                    <span>
                                        <a href="/sel-budaya/{{$selBudaya->status == 1 || $selBudaya->progress > 5 || $selBudaya->status == -1  ? 'view' : sbLinkProgress($selBudaya->progress) }}/{{$selBudaya->id}}" class="btn btn-outline-primary btn-sm 	m-btn m-btn--icon m-btn--pill">
                                            <span>
                                                <i class="la la-{{$selBudaya->status == 0 || $selBudaya->status == 1 || $selBudaya->progress > 5 ? 'eye' : 'edit'}}"></i>
                                                <span>
                                                    {{ sbProgress($selBudaya->progress, $selBudaya->status) }}
                                                </span>
                                            </span>
                                        </a>
                                    </span>
                                </td>
                                <td>
                                    <span>
                                        <a href="/sel-budaya/destroy/{{$selBudaya->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </span>
                                </td>
                                <td>
                                    {{--  <span>
                                        <a href="/rpkps/{{ $item->id }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View details">
                                            <i class="la la-eye"></i>
                                        </a>
                                        <a href="/rpkps/{{ $item->id }}/assessments" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Assessment">
                                            <i class="la la-flask"></i>
                                        </a>
                                        <a href="/rpkps/delete/{{ $item->id }}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </span>  --}}
                                </td>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
        
    </div>
</div>


@endsection
{{--  
@section('contentmodal')
    <div class="modal fade" id="newrpkps" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        New RPKPS
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/rpkps" id="addRPKPS">
                        {{ csrf_field() }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="exampleSelect1">
                                    Mata Kuliah
                                </label>
                                <select class="form-control m-input" id="" name="course_id">
                                    @foreach($courses as $course)
                                        <option value="{{ $course->id }}" required>{{ "$course->code - $course->name" }}</option>   
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group m-form__group">
                                <label for="yearpicker">
                                    Tahun Ajaran
                                </label>
                                <div class='input-group date' id='m_datepicker_1_modal'>
                                    <input type='text' class="form-control m-input" readonly  placeholder="Select date" name="year" required/>
                                    <span class="input-group-addon">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <div class="modal-footer m-form__actions">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary" form="addRPKPS">
                        Create
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection  --}}

@section('contentscript')
    <script>
   
        var DatatableHtmlTableDemo = function() {
        //== Private functions

            // demo initializer
            var demo = function() {

                var datatable = $('.m-datatable').mDatatable({
                    data: {
                        saveState: {cookie: false},
                    },
                    search: {
                        input: $('#generalSearch'),
                    },
                    columns: [
                        {
                            field: 'Nomer Registrasi',
                            width: 140,
                            textAlign: 'center',
                            sortable: !1
                        },
                        {
                            field: 'Tim',
                            width: 140
                            
                        },
                        {
                            field: 'Status',
                            width: 140,
                            textAlign: 'center'
                            
                        },
                        {
                            field: 'Tindak Lanjut',
                            width: 150,
                            textAlign: 'center'
                            
                        },
                        {
                            field: 'Hapus',
                            width: 70,
                            textAlign: 'center'
                            
                        },
                        {
                            field: 'Course ID',
                            width: 120
                            
                        },
                        {
                            field: 'Course',
                            width: 600                      
                        },
                        {
                            field: "Actions",
                            sortable: false,
                            overflow: "visible",
                            width: 150,
                            textAlign: 'center' 
                        },
                    ]
                });
            };

            return {
                //== Public functions
                init: function() {
                // init dmeo
                demo();
                },
            };
        }();

        jQuery(document).ready(function() {
        DatatableHtmlTableDemo.init();
        });
        
    </script>

    @include ('footer')

@endsection