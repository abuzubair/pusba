@extends('layouts.body')

@section('content')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-subheader ">
          <div class="d-flex align-items-center">
              <div class="mr-auto">
                  <h3 class="m-subheader__title ">
                      Sel Budaya
                  </h3>
              </div>
          </div>
      </div>
      <!-- END: Subheader -->
      <div class="m-content">
        <div class="row">
          
          <div class="col-lg-6">
            @include('displays.register')
            @include('displays.analisa')
            @include('displays.perencanaan')

            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--tab">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                      <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                      Melaksanakan Ide Perbaikan
                    </h3>
                  </div>
                </div>
              </div>
              <!--begin::Form-->
              <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/sel-budaya/pelaksanaan/{{$selBudaya->id}}" id="perencanaan">

                {{ csrf_field() }}
                <div class="m-portlet__body">

                  <div class="form-group m-form__group">
                    <label for="tantangan">
                       Tantangan
                    </label>
                    <textarea name="tantangan" class="form-control m-input" id="tantangan" rows="8">{{$selBudaya->pelaksanaan->tantangan}}</textarea>
                    @include('layouts.errors-form', ['field' => 'tantangan'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="solusi">
                       Solusi / Strategi
                    </label>
                    <textarea name="solusi" class="form-control m-input" id="solusi" rows="8">{{$selBudaya->pelaksanaan->solusi}}</textarea>
                    @include('layouts.errors-form', ['field' => 'solusi'])
                  </div>
                  
                </div>
              </form>

              @component('displays.upload', ['selBudaya' => $selBudaya, 'value' => 4])
              @endcomponent

              <div class="m-form m-form--fit m-form--label-align-right">
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="perencanaan">
                      Proses
                    </button>
                    <a href="/sel-budaya" class="btn btn-danger m-btn m-btn--custom float-left" role="button">Batal</a>
                  </div>
                </div>
              </div>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>

        </div>
      </div>
  </div>
@endsection

@section('contentmodal')

@endsection

@section('contentscript')

  @include ('footer')

@endsection
