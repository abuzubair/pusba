@extends('layouts.body')

@section('content')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-subheader ">
          <div class="d-flex align-items-center">
              <div class="mr-auto">
                  <h3 class="m-subheader__title ">
                      Sel Budaya
                  </h3>
              </div>
          </div>
      </div>
      <!-- END: Subheader -->
      <div class="m-content">
        <div class="row">
          
          <div class="col-lg-6">
            @include('displays.register')
            @include('displays.analisa')

            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--tab">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                      <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                      Perencanaan Ide Perbaikan
                    </h3>
                  </div>
                </div>
              </div>
              <!--begin::Form-->
              <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/sel-budaya/time/{{$selBudaya->id}}" id="time">

                {{ csrf_field() }}
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-5 col-sm-12">
                            Menentukan Project Budaya KPP
                        </label>
                        <div class="col-lg-7 col-sm-12">
                            <div class="input-daterange input-group" id="datepicker_1">
                                <input type="text" class="form-control m-input" name="project-start" value="{{displayDate($selBudaya->perencanaan->project_start)}}"/>
                                <span class="input-group-addon">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                                <input type="text" class="form-control" name="project-end" value="{{displayDate($selBudaya->perencanaan->project_end)}}"/>
                            </div>
                            @include('layouts.errors-form', ['field' => 'project-start'])
                            @include('layouts.errors-form', ['field' => 'project-end'])
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-5 col-sm-12">
                            Analisa Masalah
                        </label>
                        <div class="col-lg-7 col-sm-12">
                            <div class="input-daterange input-group" id="datepicker_2">
                                <input type="text" class="form-control m-input" name="analisa-start" value="{{displayDate($selBudaya->perencanaan->analysis_start)}}"/>
                                <span class="input-group-addon">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                                <input type="text" class="form-control" name="analisa-end" value="{{displayDate($selBudaya->perencanaan->analysis_end)}}"/>
                            </div>
                            @include('layouts.errors-form', ['field' => 'analisa-start'])
                            @include('layouts.errors-form', ['field' => 'analisa-end'])
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-5 col-sm-12">
                            Menentukan Ide Perbaikan
                        </label>
                        <div class="col-lg-7 col-sm-12">
                            <div class="input-daterange input-group" id="datepicker_3">
                                <input type="text" class="form-control m-input" name="perencanaan-start" value="{{displayDate($selBudaya->perencanaan->planning_start)}}"/>
                                <span class="input-group-addon">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                                <input type="text" class="form-control" name="perencanaan-end" value="{{displayDate($selBudaya->perencanaan->planning_end)}}"/>
                            </div>
                            @include('layouts.errors-form', ['field' => 'perencanaan-start'])
                            @include('layouts.errors-form', ['field' => 'perencanaan-end'])
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-5 col-sm-12">
                            Melaksanakan Ide Perbaikan
                        </label>
                        <div class="col-lg-7 col-sm-12">
                            <div class="input-daterange input-group" id="datepicker_4">
                                <input type="text" class="form-control m-input" name="pelaksanaan-start" value="{{displayDate($selBudaya->perencanaan->execution_start)}}"/>
                                <span class="input-group-addon">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                                <input type="text" class="form-control" name="pelaksanaan-end" value="{{displayDate($selBudaya->perencanaan->execution_end)}}"/>
                            </div>
                            @include('layouts.errors-form', ['field' => 'pelaksanaan-start'])
                            @include('layouts.errors-form', ['field' => 'pelaksanaan-end'])
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-5 col-sm-12">
                            Evaluasi Hasil
                        </label>
                        <div class="col-lg-7 col-sm-12">
                            <div class="input-daterange input-group" id="datepicker_5">
                                <input type="text" class="form-control m-input" name="evaluasi-start" value="{{displayDate($selBudaya->perencanaan->evaluation_start)}}"/>
                                <span class="input-group-addon">
                                    <i class="la la-ellipsis-h"></i>
                                </span>
                                <input type="text" class="form-control" name="evaluasi-end" value="{{displayDate($selBudaya->perencanaan->evaluation_end)}}"/>
                            </div>
                            @include('layouts.errors-form', ['field' => 'evaluasi-start'])
                            @include('layouts.errors-form', ['field' => 'evaluasi-end'])
                        </div>
                    </div>

                </div>

                <div id="charthead" class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="time">
                      Update Gantt Chart
                    </button>
                  </div>
                </div>

                <div class="form-group m-form__group">

                    <div id="chartdiv"></div>
                    <style>
                        #chartdiv {
                            width: 100%;
                            height: 500px;
                        }
                    </style>

                </div>
                
              </form>
              <!--end::Form-->
              <!--begin::Form-->
              <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/sel-budaya/perencanaan/{{$selBudaya->id}}" id="analisa">

                {{ csrf_field() }}
                <div class="m-portlet__body">

                  <div class="form-group m-form__group">
                    <label for="analisa">
                      Analisa Akar Penyebab Perilaku Tidak Mendukung
                    </label>
                    <textarea name="analisa" class="form-control m-input" id="analisa" rows="8">{{$selBudaya->perencanaan->analisa}}</textarea>
                    @include('layouts.errors-form', ['field' => 'analisa'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="rencana">
                      Rencana & Implementasi Solusi
                    </label>
                    <textarea name="rencana" class="form-control m-input" id="rencana" rows="8">{{$selBudaya->perencanaan->rencana}}</textarea>
                    @include('layouts.errors-form', ['field' => 'rencana'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="pihak-terlibat">
                      Pihak-Pihak Yang Terlibat & PIC Utama Pelaksanaan Solusi
                    </label>
                    <textarea name="pihak-terlibat" class="form-control m-input" id="pihak-terlibat" rows="8">{{$selBudaya->perencanaan->pihak}}</textarea>
                    @include('layouts.errors-form', ['field' => 'pihak-terlibat'])
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="analisa">
                      Proses
                    </button>
                    <a href="/sel-budaya" class="btn btn-danger m-btn m-btn--custom float-left" role="button">Batal</a>
                  </div>
                </div>
              </form>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>

        </div>
      </div>
  </div>
@endsection

@section('contentmodal')

@endsection

@section('contentscript')

  @include ('footer')
  <script>



  </script>
@endsection
