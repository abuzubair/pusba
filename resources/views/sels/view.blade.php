@extends('layouts.body')

@section('content')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-subheader ">
          <div class="d-flex align-items-center">
              <div class="mr-auto">
                  <h3 class="m-subheader__title ">
                      Dashboard
                  </h3>
              </div>
          </div>
      </div>
      <!-- END: Subheader -->
      <div class="m-content">
        <div class="row">
          <div class="col-lg-6 tinjauan">
            @includeWhen($selBudaya->progress > 0, 'displays.register')
            @includeWhen($selBudaya->progress > 1, 'displays.analisa')
            @includeWhen($selBudaya->progress > 2, 'displays.perencanaan')
            @includeWhen($selBudaya->progress > 3, 'displays.pelaksanaan')
            @includeWhen($selBudaya->progress > 4, 'displays.evaluasi')
            <div class="m-portlet m-portlet--tab">
                <div class="m-form m-form--fit m-form--label-align-right">
  
                  {{ csrf_field() }}
  
                  <div class="m-portlet__foot m-portlet__foot--fit">
                    @if ($selBudaya->progress > 5)
                    
                    <div class="m-form__actions">
                      <a class="btn btn-accent m-btn m-btn--custom float-right" href="/tinjauan/pdf/{{$selBudaya->id}}">Download</a>
                    </div>
                    @endif
                  </div>
                </div>
       
              </div>
          </div>

        </div>
      </div>
  </div>
@endsection

@section('contentmodal')

@endsection

@section('contentscript')

  @include ('footer')

@endsection
